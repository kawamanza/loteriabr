Rails.application.routes.draw do
  get 'lotofacil/last_results' => 'lottery/lotofacil#last_results'
  get 'lotofacil' => 'lottery/lotofacil#index', as: :lotofacil

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'ball/:number' => 'custom_images#ball'
end
