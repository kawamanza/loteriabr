module Lottery
  class LotofacilParser
    attr_reader :local_file_path

    def initialize
      @local_file_path = Rails.root.join("tmp/D_lotfac.zip")
    end

    def load_file
      if File.exists?(local_file_path) && File.new(local_file_path).mtime >= Date.today.to_time
        Rails.logger.info "#{ local_file_path } updated"
      else
        File.open(local_file_path, "wb") do |f|
          content = HTTParty.get('http://www1.caixa.gov.br/loterias/_arquivos/loterias/D_lotfac.zip', headers: { 'Cookie': 'security=true'})
          f.write content
        end
        Rails.logger.info "#{ local_file_path } loaded"
      end
    end

    def html_content
      load_file
      @html_content ||= Zip::ZipFile.new(local_file_path).read('D_LOTFAC.HTM')
    end
  end
end
