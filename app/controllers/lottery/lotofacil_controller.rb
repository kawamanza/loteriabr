class Lottery::LotofacilController < ApplicationController
  def last_results
    @lottery = ::Lottery::LotofacilParser.new
    render "last_results", layout: false
  end

  # GET /lotofacil
  def index
  end
end
